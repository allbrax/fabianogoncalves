import { Component, OnInit } from '@angular/core';
import { RankService } from './rank.service';
import { ResponseRanks } from './rank.model';

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
  styleUrls: ['./rank.component.css']
})
export class RankComponent implements OnInit {

  responseRank : ResponseRanks;

  constructor(private rankService: RankService) { }

  ngOnInit() {
    this.rankService.getRanks()
      .subscribe(res => this.responseRank = res);
  }
}