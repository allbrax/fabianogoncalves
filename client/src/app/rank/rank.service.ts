import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RankComponent } from './rank.component';
import { Rank, ResponseRanks, ResponseRank, RequestCreateRank } from './rank.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RankService {

  private url = "http://127.0.0.1:8000/api/ranking";

  constructor(private http: HttpClient) { }

  getRanks(): Observable<ResponseRanks>{
    return this.http.get<ResponseRanks>(this.url);
  }

  createRank(request: RequestCreateRank): Observable<Rank>{
    return this.http.post<Rank>(this.url, request);
  }

  getRank(id: string): Observable<ResponseRank>{
    const _url = `${this.url}/${id}`;
    return this.http.get<ResponseRank>(_url);
  } 

  updateRank(id: string, request: Rank): Observable<ResponseRank>{
    const _url = `${this.url}/${id}`;
    return this.http.put<ResponseRank>(_url, request);
  }
}
