export interface Rank {
    id: number;
    player: string;
    matches: string;
    wins: string;
}

export interface ResponseRanks { 
    data: Rank[];
}

export interface RequestCreateRank {
  player: string;
  matches: string;
  wins: string;
}

export interface ResponseRank { 
  data: Rank;
}