import { Component, OnInit } from '@angular/core';
import { RequestCreateRank, ResponseRank } from '../rank.model';
import { RankService } from '../rank.service';

@Component({
  selector: 'app-create-rank',
  templateUrl: './create-rank.component.html',
  styleUrls: ['./create-rank.component.css']
})
export class CreateRankComponent implements OnInit {

  request: RequestCreateRank = {
    player: '',
    matches: '',
    wins: ''
  }

  response: ResponseRank; 

  constructor(private rankService: RankService) { }

  ngOnInit() {
  }

  save(){
    this.rankService.createRank(this.request)
      .subscribe( res => this.response = res);
  }
}