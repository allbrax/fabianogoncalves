import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RankComponent } from './rank/rank.component';
import { CreateRankComponent } from './rank/create-rank/create-rank.component';
import { UpdateRankComponent } from './rank/update-rank/update-rank.component';


const routes: Routes = [
  {path:'rank', component: RankComponent},
  {path:'rank/create', component: CreateRankComponent },
  {path:'rank/update/:id', component: UpdateRankComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
