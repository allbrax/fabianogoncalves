import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RankComponent } from './rank/rank.component';
import { HttpClientModule } from '@angular/common/http';
import { CreateRankComponent } from './rank/create-rank/create-rank.component';
import { FormsModule } from "@angular/forms";
import { UpdateRankComponent } from './rank/update-rank/update-rank.component';

@NgModule({
  declarations: [
    AppComponent,
    RankComponent,
    CreateRankComponent,
    UpdateRankComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
