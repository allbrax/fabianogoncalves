<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ranking;
use App\Http\Resources\RankingResource;

class RankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RankingResource::collection(Ranking::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ranking = Ranking::create([
            'player'    =>$request->player,
            'matches'   =>$request->matches,
            'wins'      =>$request->wins,
        ]);
        return new RankingResource($ranking);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ranking $ranking)
    {
        return new RankingResource($ranking);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ranking = Ranking::findOrFail($id);
        $ranking->matches = $request->matches;
        $ranking->wins    = $request->wins;
        $ranking->save();

        return new RankingResource($ranking);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ranking = Ranking::findOrFail($id);
        $ranking->delete();
        return new RankingResource($ranking);
    }

    public function sumMatch($id)
    {
        $ranking = Ranking::findOrFail($id);
        $ranking->matches+=1;
        $ranking->save();
    }

    public function sumWin($id)
    {
        $ranking = Ranking::findOrFail($id);
        $ranking->wins+=1;
        $ranking->save();
    }
}
